using System;
using Extensibility;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.CommandBars;
using System.Resources;
using System.Reflection;
using System.Globalization;
using WinApi;

namespace EasyAddin
{
	/// <summary>用于实现外接程序的对象。</summary>
	/// <seealso class='IDTExtensibility2' />
	public class Connect : IDTExtensibility2, IDTCommandTarget
	{
        string strBarName = "EasyAddin";
        string strProg = "EasyAddin.Connect.";
        string strCmdName = "ShellMenu";

        string strPrjctCmdName = "ShellMenuProject";
        string strProjBarName = "EasyAddinProject";
        Command command = null;
        Command cmdProj = null;
        Command cmdSln = null;
		/// <summary>实现外接程序对象的构造函数。请将您的初始化代码置于此方法内。</summary>
		public Connect()
		{
		}

		/// <summary>实现 IDTExtensibility2 接口的 OnConnection 方法。接收正在加载外接程序的通知。</summary>
		/// <param term='application'>宿主应用程序的根对象。</param>
		/// <param term='connectMode'>描述外接程序的加载方式。</param>
		/// <param term='addInInst'>表示此外接程序的对象。</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
		{
			_applicationObject = (DTE2)application;
            _addInInstance = (AddIn)addInInst;
            CreateCmdBars();
            CreateProjectCmd("Project", strPrjctCmdName, out cmdProj);
            CreateProjectCmd("Solution", strPrjctCmdName, out cmdProj);
            CreateProjectCmd("Item", strPrjctCmdName, out cmdProj);
		}

		/// <summary>实现 IDTExtensibility2 接口的 OnDisconnection 方法。接收正在卸载外接程序的通知。</summary>
		/// <param term='disconnectMode'>描述外接程序的卸载方式。</param>
		/// <param term='custom'>特定于宿主应用程序的参数数组。</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnDisconnection(ext_DisconnectMode disconnectMode, ref Array custom)
        {
            if (disconnectMode == ext_DisconnectMode.ext_dm_UserClosed)
            {
                DeleteCmdBars();
            }
		}

		/// <summary>实现 IDTExtensibility2 接口的 OnAddInsUpdate 方法。当外接程序集合已发生更改时接收通知。</summary>
		/// <param term='custom'>特定于宿主应用程序的参数数组。</param>
		/// <seealso class='IDTExtensibility2' />		
		public void OnAddInsUpdate(ref Array custom)
		{
		}

		/// <summary>实现 IDTExtensibility2 接口的 OnStartupComplete 方法。接收宿主应用程序已完成加载的通知。</summary>
		/// <param term='custom'>特定于宿主应用程序的参数数组。</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnStartupComplete(ref Array custom)
		{
		}

		/// <summary>实现 IDTExtensibility2 接口的 OnBeginShutdown 方法。接收正在卸载宿主应用程序的通知。</summary>
		/// <param term='custom'>特定于宿主应用程序的参数数组。</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnBeginShutdown(ref Array custom)
		{
		}
		
		/// <summary>实现 IDTCommandTarget 接口的 QueryStatus 方法。此方法在更新该命令的可用性时调用</summary>
		/// <param term='commandName'>要确定其状态的命令的名称。</param>
		/// <param term='neededText'>该命令所需的文本。</param>
		/// <param term='status'>该命令在用户界面中的状态。</param>
		/// <param term='commandText'>neededText 参数所要求的文本。</param>
		/// <seealso class='Exec' />
		public void QueryStatus(string commandName, vsCommandStatusTextWanted neededText, ref vsCommandStatus status, ref object commandText)
		{
			if(neededText == vsCommandStatusTextWanted.vsCommandStatusTextWantedNone)
			{
            }
            if (commandName.IndexOf(strCmdName) >= 0)
            {
                status = (vsCommandStatus)vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled;
                return;
            }
            if (commandName.IndexOf(strPrjctCmdName) >= 0)
            {
                status = (vsCommandStatus)vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled;
                return;
            }
		}

		/// <summary>实现 IDTCommandTarget 接口的 Exec 方法。此方法在调用该命令时调用。</summary>
		/// <param term='commandName'>要执行的命令的名称。</param>
		/// <param term='executeOption'>描述该命令应如何运行。</param>
		/// <param term='varIn'>从调用方传递到命令处理程序的参数。</param>
		/// <param term='varOut'>从命令处理程序传递到调用方的参数。</param>
		/// <param term='handled'>通知调用方此命令是否已被处理。</param>
		/// <seealso class='Exec' />
		public void Exec(string commandName, vsCommandExecOption executeOption, ref object varIn, ref object varOut, ref bool handled)
		{
			handled = false;
			if(executeOption == vsCommandExecOption.vsCommandExecOptionDoDefault)
			{
            }
            if (commandName.EndsWith(strCmdName))
            {
                try
                {
                    WinApi.POINT p = new POINT(0, 0);
                    try
                    {
                        WinApi.Api.GetCursorPos(ref p);
                    }
                    catch (System.Exception e)
                    {
                        _applicationObject.StatusBar.Text = "GetCursorPos:" + e.Message;
                    }
                    ExecShellMenu(_applicationObject.ActiveDocument.FullName
                        , p, new System.IntPtr(_applicationObject.MainWindow.HWnd));
                }
                catch (System.Exception e)
                {
                    _applicationObject.StatusBar.Text = "ShellMenu:" + e.Message;
                }
                handled = true;
                return;
            }
            if (commandName.EndsWith(strPrjctCmdName))
            {
                try
                {
                    WinApi.POINT p = new POINT(0, 0);
                    try
                    {
                        WinApi.Api.GetCursorPos(ref p);
                    }
                    catch (System.Exception e)
                    {
                        _applicationObject.StatusBar.Text = "GetCursorPos:" + e.Message;
                    }
                    //object[] os = (object[])_applicationObject.ActiveSolutionProjects;
                    //if (os.Length <= 0)
                    //{
                    //    ExecShellMenu(_applicationObject.Solution.FullName
                    //    , p, new System.IntPtr(_applicationObject.MainWindow.HWnd));
                    //    return;
                    //}
                    //Project proj =  (Project)os[0];
                    if (_applicationObject.SelectedItems == null)
                    {
                        return;
                    }
                    SelectedItem si = _applicationObject.SelectedItems.Item(1);
                    if (si.Project != null)
                    {
                        ExecShellMenu(si.Project.FullName
                            , p, new System.IntPtr(_applicationObject.MainWindow.HWnd));
                    }
                    else if (si.ProjectItem != null)
                    {
                        ExecShellMenu(si.ProjectItem.get_FileNames(0)
                            , p, new System.IntPtr(_applicationObject.MainWindow.HWnd));
                    }
                    else
                    {
                        ExecShellMenu(_applicationObject.Solution.FullName
                        , p, new System.IntPtr(_applicationObject.MainWindow.HWnd));
                    }
                }
                catch (System.Exception e)
                {
                    _applicationObject.StatusBar.Text = "ShellMenu:" + e.Message;
                }
                handled = true;
                return;
            }
		}
		private DTE2 _applicationObject;
		private AddIn _addInInstance;

        private void CreateProjectCmd(string parentBar, string cmdName, out Command cmdObj)
        {
            string toolBarName = parentBar;
            object[] contextGUIDS = new object[] { };
            Commands2 commands = (Commands2)_applicationObject.Commands;
            Microsoft.VisualStudio.CommandBars.CommandBar cwCommandBar =
                ((Microsoft.VisualStudio.CommandBars.CommandBars)_applicationObject.CommandBars)[parentBar];
            cmdObj = null;
            //如果希望添加多个由您的外接程序处理的命令，可以重复此 try/catch 块，
            //  只需确保更新 QueryStatus/Exec 方法，使其包含新的命令名。
            bool bCmdNewCreated = false;
            try
            {
                //将一个命令添加到 Commands 集合:
                try
                {
                    cmdObj = commands.Item(this.strProg + cmdName, -1);
                }
                catch (System.Exception e)
                {
                    cmdObj = null;
                    _applicationObject.StatusBar.Text = "It's About To Create Project Command";
                }
                if (cmdObj == null)
                {
                    cmdObj = commands.AddNamedCommand2(_addInInstance, cmdName,
                        "ShellMenu", "Display explorer menu of file", true, 0,
                        ref contextGUIDS,
                        (int)vsCommandStatus.vsCommandStatusSupported + (int)vsCommandStatus.vsCommandStatusEnabled,
                        (int)vsCommandStyle.vsCommandStylePictAndText,
                        vsCommandControlType.vsCommandControlTypeButton);
                    bCmdNewCreated = true;
                }
                _applicationObject.StatusBar.Text = "Command Created.";

                DeleteControl(cwCommandBar, "ShellMenu");
                DeleteControl(cwCommandBar, strProjBarName);

                if (cmdObj != null)
                {
                    cmdObj.AddControl(cwCommandBar, 1);
                }
            }
            catch (System.ArgumentException ex)
            {
                //如果出现此异常，原因很可能是由于具有该名称的命令
                //  已存在。如果确实如此，则无需重新创建此命令，并且
                //  可以放心忽略此异常。
                _applicationObject.StatusBar.Text = "Argument:" + ex.Message;
            }
        }

        private void DeleteControl(Microsoft.VisualStudio.CommandBars.CommandBar bar, string ctrlName)
        {
            foreach (object o in bar.Controls)
            {
                try
                {
                    CommandBarControl easyMenu = o as CommandBarControl;
                    if (easyMenu != null && easyMenu.Caption == ctrlName)
                    {
                        easyMenu.Delete(false);
                    }
                    easyMenu = null;
                }
                catch (System.Exception e)
                {
                	
                }
            }
        }

        private void CreateCmdBars()
        {
            object[] contextGUIDS = new object[] { };
            Commands2 commands = (Commands2)_applicationObject.Commands;
            string toolsMenuName = "Code Window";


            //如果希望添加多个由您的外接程序处理的命令，可以重复此 try/catch 块，
            //  只需确保更新 QueryStatus/Exec 方法，使其包含新的命令名。
            bool bCmdNewCreated = false;
            try
            {
                //查找 MenuBar 命令栏，该命令栏是容纳所有主菜单项的顶级命令栏:
                Microsoft.VisualStudio.CommandBars.CommandBar cwCommandBar =
                    ((Microsoft.VisualStudio.CommandBars.CommandBars)_applicationObject.CommandBars)[toolsMenuName];
                //将一个命令添加到 Commands 集合:
                try
                {
                    command = commands.Item(this.strProg + strCmdName, -1);
                }
                catch (System.Exception e)
                {
                    command = null;
                    _applicationObject.StatusBar.Text = "It's About To Create Command";
                }
                if (command == null)
                {
                    command = commands.AddNamedCommand2(_addInInstance, strCmdName,
                        "ShellMenu", "Display explorer menu of file", true, 0,
                        ref contextGUIDS,
                        (int)vsCommandStatus.vsCommandStatusSupported + (int)vsCommandStatus.vsCommandStatusEnabled,
                        (int)vsCommandStyle.vsCommandStylePictAndText,
                        vsCommandControlType.vsCommandControlTypeButton);
                    bCmdNewCreated = true;
                }
                _applicationObject.StatusBar.Text = "Command Created.";

                CommandBar easyBar = null;
                CommandBarControl easyMenu = null;
                try
                {
                    foreach (object o in cwCommandBar.Controls)
                    {
                        easyMenu = o as CommandBarControl;
                        if (easyMenu != null && easyMenu.Caption == strBarName)
                        {
                            CommandBars allBar = (CommandBars)_applicationObject.CommandBars;
                            easyBar = allBar[strBarName];
                            if (bCmdNewCreated)
                            {
                                easyBar.Delete();
                                easyBar = null;
                            }
                            break;
                        }
                        easyMenu = null;
                    }
                    if (easyBar == null)
                    {
                        easyBar = (CommandBar)_applicationObject.Commands.AddCommandBar(strBarName,
                            vsCommandBarType.vsCommandBarTypeMenu,
                            cwCommandBar, 1);
                    }
                    else
                    {
                        easyBar = null;
                    }
                }
                catch (System.Exception)
                {
                    easyBar = null;
                }
                if ((command != null) && (easyBar != null))
                {
                    command.AddControl(easyBar, 1);
                }
            }
            catch (System.ArgumentException ex)
            {
                //如果出现此异常，原因很可能是由于具有该名称的命令
                //  已存在。如果确实如此，则无需重新创建此命令，并且
                //  可以放心忽略此异常。
                _applicationObject.StatusBar.Text = "Argument:" + ex.Message;
            }
        }

        private void DeleteCmdBars()
        {
            try
            {
                if (command != null)
                {
                    command.Delete();
                    _applicationObject.StatusBar.Text = "Command deleted!";
                    CommandBar cwCommandBar =
                       ((Microsoft.VisualStudio.CommandBars.CommandBars)_applicationObject.CommandBars)["Code Window"];
                    DeleteControl(cwCommandBar, "EasyAddin");
                }
                if (this.cmdProj != null)
                {
                    this.cmdProj.Delete();
                    _applicationObject.StatusBar.Text = "Project command deleted!";
                    CommandBar cwCommandBar =
                       ((Microsoft.VisualStudio.CommandBars.CommandBars)_applicationObject.CommandBars)["Project"];
                    DeleteControl(cwCommandBar, "ShellMenu"); 
                    
                    cwCommandBar =
                        ((Microsoft.VisualStudio.CommandBars.CommandBars)_applicationObject.CommandBars)["Solution"];
                    DeleteControl(cwCommandBar, "ShellMenu");

                    cwCommandBar =
                        ((Microsoft.VisualStudio.CommandBars.CommandBars)_applicationObject.CommandBars)["Item"];
                    DeleteControl(cwCommandBar, "ShellMenu");
                }
                //_applicationObject.Commands.RemoveCommandBar(strBarName);
                bool bLoop = true;
                while (bLoop)
                {
                    try
                    {
                        CommandBar cwCommandBar =
                           ((Microsoft.VisualStudio.CommandBars.CommandBars)_applicationObject.CommandBars)[strBarName];
                        if (cwCommandBar != null)
                        {
                            _applicationObject.Commands.RemoveCommandBar(cwCommandBar);
                        }
                    }
                    catch (System.Exception)
                    {
                        bLoop = false;
                    }
                }
            }
            catch (System.Exception e)
            {
                _applicationObject.StatusBar.Text = "DeleteCmdBars:" + e.Message;
            }
        }
        public bool ExecShellMenu(string file, WinApi.POINT point, IntPtr hwnd)
        {
            if (!System.IO.Directory.Exists(file) &&
                !System.IO.File.Exists(file))
            {
                return false;
            }
            WinApi.IShellFolder shlFld = null;
            int iHrslt = WinApi.Api.SHGetDesktopFolder(ref shlFld);
            uint pchEaten = 0;
            uint pdwAttributes = 0;
            IntPtr pidl;
            System.Runtime.InteropServices.Marshal.ThrowExceptionForHR(iHrslt);

            iHrslt = shlFld.ParseDisplayName(hwnd, IntPtr.Zero,
                file, ref pchEaten, out pidl, ref pdwAttributes);
            System.Runtime.InteropServices.Marshal.ThrowExceptionForHR(iHrslt);

            Guid IID_IShellFolder = new Guid("000214E6-0000-0000-C000-000000000046");
            Guid IID_IContextMenu = new Guid("000214E4-0000-0000-c000-000000000046");

            object o = null;

            WinApi.IShellFolder parentSF = null;
            IntPtr iLastIdl = new IntPtr();
            iHrslt = WinApi.Api.SHBindToParent(pidl, ref IID_IShellFolder, out o, ref iLastIdl);
            System.Runtime.InteropServices.Marshal.ThrowExceptionForHR(iHrslt);
            parentSF = (WinApi.IShellFolder)o;

            WinApi.IContextMenu ctxMnu = null;
            IntPtr[] pidls = new IntPtr[1];
            pidls[0] = iLastIdl;
            uint rv = 0;
            iHrslt = parentSF.GetUIObjectOf(IntPtr.Zero, (uint)pidls.Length, pidls,
                ref IID_IContextMenu, ref rv, out o);
            System.Runtime.InteropServices.Marshal.ThrowExceptionForHR(iHrslt);
            ctxMnu = (WinApi.IContextMenu)o;

            IntPtr pMenu = WinApi.Api.CreatePopupMenu();
            iHrslt = ctxMnu.QueryContextMenu(pMenu, 0, 1, 30000,
                (uint)(WinApi.ICM_QCM_FLAG.CMF_EXPLORE | WinApi.ICM_QCM_FLAG.CMF_NORMAL));
            System.Runtime.InteropServices.Marshal.ThrowExceptionForHR(iHrslt);

            int iCmdID = WinApi.Api.TrackPopupMenu(pMenu, (uint)WinApi.Api.TrackPopupMenuFlag.TPM_RETURNCMD,
                point.x, point.y, 0, hwnd, IntPtr.Zero);

            if (iCmdID >= 1)
            {
                try
                {
                    WinApi.CMINVOKECOMMANDINFOEX invoke = new WinApi.CMINVOKECOMMANDINFOEX();
                    invoke.cbSize = System.Runtime.InteropServices.Marshal.SizeOf(typeof(WinApi.CMINVOKECOMMANDINFOEX));
                    invoke.lpVerb = (IntPtr)(iCmdID - 1);
                    invoke.lpDirectory = string.Empty;
                    invoke.fMask = 0;
                    invoke.ptInvoke = point;
                    invoke.nShow = 1;
                    ctxMnu.InvokeCommand(ref invoke);
                    return true;
                }
                catch (System.Exception ex)
                {
                }
            }

            return false;
        }
	}
}