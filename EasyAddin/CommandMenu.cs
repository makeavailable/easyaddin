using System;
using System.Collections.Generic;
using System.Text;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.CommandBars;

namespace EasyAddin
{
    class MyCommand
    {
        private DTE2 _applicationObject;
        private AddIn _addInInstance;
        string name;
        string strButtonText;
        string tooltip;
        bool MSOButton = true;
        object Bitmap = 0;
        object[] contextGUIDS = new object[] { };
        int vsCommandStatusValue = (int)(vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled);
        int CommandStyleFlags = (int)vsCommandStyle.vsCommandStylePictAndText;
        vsCommandControlType ControlType = vsCommandControlType.vsCommandControlTypeButton;

        Command command = null;

        public MyCommand(DTE2 applicationObject, AddIn addin, string name, string buttonText, string tooltip)
        {
            this._applicationObject = applicationObject;
            this._addInInstance = addin;
            this.name = name;
            this.strButtonText = buttonText;
            this.tooltip = tooltip;
        }

        public bool create()
        {
            Commands2 commands = (Commands2)_applicationObject.Commands;
            try
            {
                this.command = commands.Item(this._addInInstance.ProgID + this.name, -1);
            }
            catch (System.Exception e)
            {
                this.command = null;
                _applicationObject.StatusBar.Text = "It's About To Create Command";
            }
            if (command == null)
            {
                command = commands.AddNamedCommand2(_addInInstance, this.name,
                    this.strButtonText, this.tooltip, this.MSOButton, this.Bitmap,
                    ref contextGUIDS, this.vsCommandStatusValue,
                    this.CommandStyleFlags, this.ControlType);
  
            }

            return true;
        }
        public bool clear()
        {
            if (null != command)
            {
                command.Delete();
                command=null;
            }
            return true;
        }

        public bool addToMenu(MyMenu menu)
        {
            return true;
        }
    }

    class MyMenu
    {
        private DTE2 _applicationObject;
        private AddIn _addInInstance;

        Microsoft.VisualStudio.CommandBars.CommandBar containerCommandBar;
        CommandBar secondCommandBar;
        string containerName;
        string name;
        System.Collections.Generic.List<MyCommand> commands;

        public MyMenu(DTE2 applicationObject, AddIn addin, string parent, string name)
        {
            this._applicationObject = applicationObject;
            this._addInInstance = addin;

            this.containerName = parent;
            this.name = name;

            this.init();
        }

        private bool init()
        {
            containerCommandBar = ((Microsoft.VisualStudio.CommandBars.CommandBars)_applicationObject.CommandBars)[this.containerName];
            if (string.IsNullOrEmpty(this.name))
            {
                this.secondCommandBar = this.containerCommandBar;
            }
            else
            {
                CommandBarControl easyMenu = null;
                foreach (object o in containerCommandBar.Controls)
                {
                    easyMenu = o as CommandBarControl;
                    if (easyMenu != null && easyMenu.Caption == this.name)
                    {
                        CommandBars allBar = (CommandBars)_applicationObject.CommandBars;
                        secondCommandBar = allBar[this.name];
                        break;
                    }
                    easyMenu = null;
                }
                if (null == easyMenu)
                {
                    secondCommandBar = (CommandBar)_applicationObject.Commands.AddCommandBar(this.name,
                        vsCommandBarType.vsCommandBarTypeMenu,
                        this.containerCommandBar, 1);
                }
            }

            return true;
        }

        public bool 

        public System.Collections.Generic.List<MyCommand> Commands
        {
            get{
                if (null == commands)
                {
                    commands = new System.Collections.Generic.List<MyCommand>();
                }

                return commands;
            }
        }
    }
}
